import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'note.dart';

class Day extends ChangeNotifier{

  DateTime time;
  List<Note> notes;

  Day({this.time}) : assert(time != null);

  addNote(Note note){
    this.notes.add(note);

    notifyListeners();
  }

  setNotes(List<Note> note){
    this.notes = note;

    notifyListeners();
  }

  getNotes() => this.notes;

  getTimeOfDay() => DateFormat('hh:mm:ss').format(this.time);

  getDay()  =>DateFormat('EEEE, dd/MM/yyyy').format(this.time);

  setTimeOfDay(DateTime time) {
    this.time = time;

    notifyListeners();
  }
}
