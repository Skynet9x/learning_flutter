import 'package:flutter/cupertino.dart';

class Note extends ChangeNotifier{

  String note;

  setNoteContent(String note){
    this.note = note;
  }

  getNoteContent() => this.note;

}