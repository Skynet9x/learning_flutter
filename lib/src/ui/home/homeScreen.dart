import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:thaos_calendar/src/custom/table_calendar/table_calendar.dart';
import 'package:thaos_calendar/src/models/day.dart';
import 'package:thaos_calendar/src/utils/color.dart';
import 'package:thaos_calendar/src/utils/dateUtils.dart';

/// Màn hình home
/// hiển thị danh sách ghi chú nếu hôm đó có
///
/// Author: Dương
class HomeScreen extends StatefulWidget {
  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {

  Day mCalendar;
  bool _didChangeDependencies = false;
  ValueNotifier<DateTime> localDate = ValueNotifier(new DateTime.now());
  ValueNotifier<DateTime> selectedDate = ValueNotifier(new DateTime.now());
  ValueNotifier<bool> nextOrPreviousPage = ValueNotifier(false);
  ValueNotifier<CalendarController> _calendarController = ValueNotifier(CalendarController());

  @override
  void initState() {
    super.initState();
  }

  @override
  void didUpdateWidget(HomeScreen oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (!_didChangeDependencies){
      _didChangeDependencies = true;
      mCalendar = Provider.of<Day>(context, listen: false);
    }

  }

  @override
  void dispose() {
    _calendarController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("re draw home");
    return _CalendarInherited (
        data: this,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            titleSpacing: 16,
            title: PreferredSize(
              child: Container(
                padding: EdgeInsets.only(top: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        print("click icon home");
                      },
                      child: Container(
                          padding: EdgeInsets.all(2),
                          width: 48,
                          height: 48,
                          child: SvgPicture.asset('assets/icons/ic_home.svg')
                      ),
                    )
                  ],
                ),
              ),
              preferredSize: Size.fromHeight(56),
            ),
            bottom: PreferredSize(
              child: Container(),
              preferredSize: Size.fromHeight(8),
            ),
            elevation: 0.5,
            actions: <Widget>[
              Container(
                padding: EdgeInsets.only(right: 16, top: 8),
                child: ReloadIconWidget(),
              )
            ],
          ),
          body: Container(
            child: Column(
              children: <Widget>[
                Container(
                    padding: EdgeInsets.only(
                        left: 24, right: 24, top: 16, bottom: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            localDate.value = getPreviousMonth(localDate.value);

                            _calendarController.value.selectPrevious();
                            print("click previous calendar, month: ${localDate.value.month}");
                          },
                          child: Container(
                              width: 48,
                              height: 48,
                              padding: EdgeInsets.all(12),
                              child: SvgPicture.asset(
                                  "assets/icons/ic_back.svg")
                          ),
                        ),
                        Expanded(
                          child: MonthAndYearWidget(),
                        ),
                        GestureDetector(
                          onTap: () {
                            localDate.value = getNextMonth(localDate.value);

                            _calendarController.value.selectNext();
                            print("click next calendar, month: ${localDate.value.month}");
                          },
                          child: Container(
                              width: 48,
                              height: 48,
                              padding: EdgeInsets.all(12),
                              child: SvgPicture.asset(
                                  "assets/icons/ic_next.svg")
                          ),
                        ),
                      ],
                    )
                ),
                Expanded(
                  child: CalendarCustomWidget(),
                )
              ],
            ),
          ),
        )
    );
  }
}

///Reload icon
class ReloadIconWidget extends StatefulWidget {
  @override
  _ReloadIconWidgetState createState() => _ReloadIconWidgetState();
}

class _ReloadIconWidgetState extends State<ReloadIconWidget> {

  DateFormat mDateFormat = DateFormat('EEEE, dd/MM/yyyy');
  DateTime oldDate;

  @override
  Widget build(BuildContext context) {
    print("rebuild reload widget");
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        GestureDetector(
          onTap: () {
            print("click icon reload");
          },
          child: Container(
              width: 48,
              height: 48,
              padding: EdgeInsets.all(8),
              child: SvgPicture.asset('assets/icons/ic_reload.svg')
          ),
        ),
        ValueListenableBuilder<DateTime>(
          valueListenable: _CalendarInherited.of(context).data.localDate,
          builder: (context, value, _) {
            return Text(
                mDateFormat.format(value),
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16
                )
            );
          },
        ),
      ],
    );
  }
}

/// month and year button
class MonthAndYearWidget extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    print("re draw month and year widget");
    return Container(
      height: 48,
      padding: EdgeInsets.only(left: 32, right: 32),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            child: Container(
              margin: EdgeInsets.only(right: 1),
              padding: EdgeInsets.only(top: 8, bottom: 8, left: 16, right: 16),
              decoration: BoxDecoration(
                  color: ColorsUtils.fromHex("#FFEEEC"),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(16),
                    bottomLeft: Radius.circular(16),
                  )
              ),
              child: ValueListenableBuilder<DateTime>(
                valueListenable: _CalendarInherited.of(context).data.localDate,
                builder: (context, value, _) {
                  return Text(
                    "Tháng ${value.month}",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: ColorsUtils.fromHex("#F74157"),
                        fontWeight: FontWeight.w700
                    ),
                  );
                },
              ),
            ),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 1),
              padding: EdgeInsets.only(top: 8, bottom: 8, left: 16, right: 16),
              decoration: BoxDecoration(
                  color: ColorsUtils.fromHex("#FFEEEC"),
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(16),
                    bottomRight: Radius.circular(16),
                  )
              ),
              child: ValueListenableBuilder<DateTime>(
                valueListenable: _CalendarInherited.of(context).data.localDate,
                builder: (context, value, _) {
                  return Text(
                    "Năm ${value.year}",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: ColorsUtils.fromHex("#F74157"),
                        fontWeight: FontWeight.w700
                    ),
                  );
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}

/// calendar inherited state
class _CalendarInherited extends InheritedWidget {

  final HomeScreenState data;

  _CalendarInherited({
    Key key,
    @required this.data,
    @required Widget child
  })
      : assert (child != null),
        super(key: key, child: child);


  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }

  static _CalendarInherited of(BuildContext context){
    return context.inheritFromWidgetOfExactType(_CalendarInherited) as _CalendarInherited;
  }
}

class CalendarCustomWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    print("reload calendar");
    return ValueListenableBuilder<DateTime>(
      valueListenable: _CalendarInherited.of(context).data.localDate,
      builder: (context, value, _) {
        return TableCalendar(
          calendarController: _CalendarInherited.of(context).data._calendarController.value,
          headerVisible: false,
          availableGestures: AvailableGestures.none,
          availableCalendarFormats: const {
            CalendarFormat.month: 'Month',
          },
          formatAnimation: FormatAnimation.slide,
          onDaySelected: (dateTime, _){
            _CalendarInherited.of(context).data.localDate.value = dateTime;
          },
        );
      },
    );
  }
}
