import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:thaos_calendar/src/models/day.dart';

import 'home/homeScreen.dart';

void main() {
  Day calendar = Day(time: new DateTime.now());

  initializeDateFormatting("en_US").then((_) {
    return runApp(
        MyApp(
          calendar: calendar,
        )
    );
  });
}

class MyApp extends StatelessWidget {

  final Day calendar;
  final DateFormat dateFormat;

  MyApp({
    this.calendar,
    this.dateFormat
  });

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<Day>.value(
              value: calendar
          )
        ],
        child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
              fontFamily: 'Lato',
              primaryColor: Colors.white,
              brightness: Brightness.light,
             ),
          home: HomeScreen(),
        )
    );
  }
}
